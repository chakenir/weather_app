require 'rakuten_web_service'
class RakutenController < ApplicationController

  RakutenWebService.configure do |c|
    c.application_id = '1042833640608336400'
  end

  def search
    if params[:keyword]
      @items = RakutenWebService::Ichiba::Item.search(keyword: params[:keyword])
    end
  end

end
