class YoutubeController < ApplicationController

  require 'rubygems'
  require 'google/api_client'
  require 'trollop'

  DEVELOPER_KEY = 'AIzaSyCUPWaZt2Y_wk3uVnRYOUsVi9okQGeru0s'
  YOUTUBE_API_SERVICE_NAME = 'youtube'
  YOUTUBE_API_VERSION = 'v3'

  def get_service
    client = Google::APIClient.new(
      :key => DEVELOPER_KEY,
      :authorization => nil,
      :application_name => $PROGRAM_NAME,
      :application_version => '1.0.0'
    )
    youtube = client.discovered_api(YOUTUBE_API_SERVICE_NAME, YOUTUBE_API_VERSION)

    return client, youtube
  end

  def get_data(keyword)
    opts = Trollop::options do
      opt :q, 'Search term', :type => String, :default => keyword
      opt :max_results, 'Max results', :type => :int, :default => 50
    end

    client, youtube = get_service

    begin

      search_response = client.execute!(
        :api_method => youtube.search.list,
        :parameters => {
          :part => 'snippet',
          :q => opts[:q],
          :maxResults => opts[:max_results],
          :order => opts[:order],
          :regionCode => opts[:regionCode]
        }
      )
      @videos = []
      search_response.data.items.each do |search_result|
        @videos << "#{search_result.snippet.title} (#{search_result.id.videoId})"
        puts e.result.body
      end
    end
  end

end
