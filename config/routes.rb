Rails.application.routes.draw do

  root to: 'toppage#index'
  get 'index'          => 'toppage#index'
  get 'about_us'       => 'toppage#about_us'
  get 'works'          => 'toppage#works'
  get 'rakuten_search' => 'rakuten#search'
  get 'youtube'        => 'youtube#index'

end
